# Docbook renderer

A new Docbook to HTML renderer, with the intended purpose of replacing the existing PhD render for the
php.net documentation, but also usable for standalone projects, such as blog articles or books.

## Objectives

The objectives of this project are:

 - Generate accessible and semantic HTML
 - Maintainable and extensible
 - Ease of adding links to refer to other part of the document

## Design

The current design choices of this project

 - Every tag (node) should be responsible for handling its rendering
 - Linking to different elements will be done in a secondary pass after it knows the location of each linkable element.

These choices may change as the project evolves

## ToDos

Listed in no particular order:

 - Improve README
 - Add Rendering support to most DocBook 5.1 tags
 - Add linking step
 - Add support for chunks
 - Make Psalm static analysis work
 - Improve testing
 - Set up CI
 - Change CS rules for some idiosyncrasies 
 - Add integration with [Meilisearch](https://docs.meilisearch.com/learn/getting_started/quick_start.html)

### References

MDN docs:
- DOM: https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model
- HTML: https://developer.mozilla.org/en-US/docs/Web/HTML/Reference

WAI Accessibility examples:
https://www.w3.org/WAI/ARIA/apg/example-index/

DocBooks 5.1 reference:
https://tdg.docbook.org/tdg/5.1/

Meilisearch:
https://docs.meilisearch.com/learn/getting_started/quick_start.html

## Contributing

Contributions are warmly welcomed by opening a merge request or creating an issue.

## Licensing

The code in this project is licensed under the GNU Affero General Public License v3 (AGPL-3.0).