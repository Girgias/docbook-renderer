<?php

/* Notes
DOMProcessingInstruction is a tag that starts like '<?phpdoc', '<?php', or even '<?xml' and ends with '?>'
See: https://developer.mozilla.org/en-US/docs/Web/API/ProcessingInstruction

People to contact about accessibility?
https://seirdy.one/
https://incl.ca/
*/

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;

$xml = <<<'XML'
<?xml version="1.0" encoding="utf-8"?>
<article xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xhtml">
 <example>
  <title><function>array_intersect</function> example</title>
  <programlisting role="php">
 <![CDATA[
 <?php
 $array1 = array("a" => "green", "red", "blue");
 $array2 = array("b" => "green", "yellow", "red");
 $result = array_intersect($array1, $array2);
 print_r($result);
 ?>
 ]]>
  </programlisting>
 <!-- &example.outputs; -->
  <screen role="php">
 <![CDATA[
 Array
 (
     [a] => green
     [0] => red
 )
 ]]>
  </screen>
 </example>
</article>
XML;

require 'vendor/autoload.php';

$d = new DOMRenderingDocument($xml);
$state = new HierarchicalState();

$content = $d->render($state);
var_dump($content);
