<?php

namespace Girgias\DocbookRender;

use Girgias\DocbookRender\Tags\AcronymTagRender;
use Girgias\DocbookRender\Tags\DateTagRenderer;
use Girgias\DocbookRender\Tags\DropTagRenderer;
use Girgias\DocbookRender\Tags\KeyComboTagRenderer;
use Girgias\DocbookRender\Tags\LinkTagRenderer;
use Girgias\DocbookRender\Tags\ProgramListingTagNoHighlightingRenderer;
use Girgias\DocbookRender\Tags\QandARenderer;
use Girgias\DocbookRender\Tags\ScreenTagRenderer;
use Girgias\DocbookRender\Tags\Sections\AlertRenderer;
use Girgias\DocbookRender\Tags\Sections\ArticleTagRenderer;
use Girgias\DocbookRender\Tags\Sections\SectionTagRenderer;
use Girgias\DocbookRender\Tags\Sections\TitleTagRenderer;
use Girgias\DocbookRender\Tags\SimpleListTagRenderer;
use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;
use Girgias\DocbookRender\Tags\Table\TableTagRenderer;
use Girgias\DocbookRender\Tags\TypeTagRendererWithoutLinking;
use Girgias\DocbookRender\Tags\UnknownTag;

/** @psalm-suppress PropertyNotSetInConstructor */
class DOMRenderingDocument extends \DOMDocument implements TagRenderer
{
    /** @param $getElementTagRenderer pure-Closure(\DOMElement $element):TagRenderer|null */
    public function __construct(string $xml, \Closure|null $getElementTagRenderer = null)
    {
        parent::__construct();
        $this->preserveWhiteSpace = false;
        $this->formatOutput = true;

        if ($getElementTagRenderer === null) {
            $getElementTagRenderer = $this->getTagRenderer(...);
        }
        // Define the TagRenderer search Closure for the rendering elements.
        DOMRenderingElement::$getTagRenderer = $getElementTagRenderer;
        $this->registerNodeClass(\DOMElement::class, DOMRenderingElement::class);
        $this->registerNodeClass(\DOMText::class, DOMRenderingText::class);
        $this->loadXML($xml);
    }

    public function render(RendererState $state): string
    {
        return $this->firstElementChild->render($state);
    }

    public function getTagRenderer(\DOMElement&TagRenderer $element): TagRenderer
    {
        return match ($element->tagName) {
            /* Basic content tags */
            'para', 'simpara' => new SimpleReplacementTagRender($element, 'p'),
            'emphasis' => new SimpleReplacementTagRender($element, 'em'),
            'simplelist' => new SimpleListTagRenderer($element),
            'member' => new DropTagRenderer($element),
            'itemizedlist' => new SimpleReplacementTagRender($element, 'ul'),
            'orderedlist' => new SimpleReplacementTagRender($element, 'ol'),
            'listitem' => new SimpleReplacementTagRender($element, 'li'),
            'acronym' => new AcronymTagRender($element, ['HTTP' => 'HyperText Markup Language']),
            'type' => new TypeTagRendererWithoutLinking($element),
            'date' => new DateTagRenderer($element),
            /* Table elements */
            'table', 'informaltable' => new TableTagRenderer($element),
            /* Q&A */
            'qandaset' => new QandARenderer($element),
            /* Monospace/user input */
            'command', 'filename', 'option' => new SimpleReplacementTagRender($element, 'kbd'),
            'varname' => new SimpleReplacementTagRender($element, 'var'),
            'code', 'literal' => new SimpleReplacementTagRender($element, 'code'),
            /* Programming examples */
            'programlisting' => new ProgramListingTagNoHighlightingRenderer($element),
            'screen' => new ScreenTagRenderer($element),
            /* Sections */
            'section' => new SectionTagRenderer($element, 'section'),
            'article' => new ArticleTagRenderer($element, 'article'),
            'title' => new TitleTagRenderer($element),
            'note', 'caution', 'warning', 'tip' => new AlertRenderer($element),
            /* Links */
            'link' => new LinkTagRenderer($element),
            /* Keyboard / User actions tags */
            'keycap' =>  new SimpleReplacementTagRender($element, 'kbd'),
            'keycombo' =>  new KeyComboTagRenderer($element, 'kbd'),
            default => new UnknownTag($element),
        };
    }
}
