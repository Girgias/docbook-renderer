<?php

namespace Girgias\DocbookRender;

class DOMRenderingText extends \DOMText implements TagRenderer
{
    public function render(RendererState $state): string
    {
        return $this->textContent;
    }
}
