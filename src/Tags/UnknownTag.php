<?php

namespace Girgias\DocbookRender\Tags;

use DOMElement;
use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class UnknownTag implements TagRenderer
{
    public function __construct(private readonly DOMElement $element)
    {
    }

    public function render(RendererState $state): string
    {
        var_dump($this->element);
        throw new \Exception("Cannot handle tag " . $this->element->tagName);
    }
}
