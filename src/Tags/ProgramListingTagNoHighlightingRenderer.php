<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class ProgramListingTagNoHighlightingRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement $element)
    {
    }

    public function render(RendererState $state): string
    {
        $language = $this->element->getAttribute('role');
        $content = '';
        assert(count($this->element->childNodes) === 1);
        assert($this->element->firstChild instanceof \DOMCdataSection);
        $content .= '<pre><code class="language-' . $language . '">';
        $content .= htmlspecialchars(
            ltrim($this->element->firstChild->textContent, "\r\n"),
            ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5
        );
        $content .= '</code></pre>';
        return $content;
    }
}
