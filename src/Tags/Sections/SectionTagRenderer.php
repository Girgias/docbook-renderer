<?php

namespace Girgias\DocbookRender\Tags\Sections;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\State\HierarchicalState;
use Girgias\DocbookRender\TagRenderer;
use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;

class SectionTagRenderer extends SimpleReplacementTagRender implements TagRenderer
{
    public function render(RendererState $state): string
    {
        $subState = clone $state;
        $subState->update(
            HierarchicalState::HIERARCHICAL_LEVEL,
            $subState->get(HierarchicalState::HIERARCHICAL_LEVEL) + 1
        );
        return parent::render($subState);
    }
}
