<?php

namespace Girgias\DocbookRender\Tags\Sections;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\State\HierarchicalState;
use Girgias\DocbookRender\TagRenderer;

class TitleTagRenderer implements TagRenderer
{
    private readonly string $id;
    public function __construct(
        private readonly \DOMElement&TagRenderer $element,
        /** @param array<string> $classes */
        private readonly array $classes = [],
        string $id = '',
    ) {
        if ($id === '' && $this->element->hasAttribute('xml:id')) {
            $id = $element->getAttribute('xml:id');
        }
        $this->id = $id;
    }

    public function render(RendererState $state): string
    {
        $level = $state->get(HierarchicalState::HIERARCHICAL_LEVEL);
        $tag = match ($level) {
            1,2,3,4,5,6 => 'h' . $level,
            default => 'h6',
        };

        $id = '';
        if ($this->id) {
            $id = ' id="' . $this->id . '"';
        }
        $classes = '';
        if ($this->classes) {
            $classes = ' class="' . implode(' ', $this->classes) . '"';
        }
        $content = '<' . $tag . $id . $classes . '>';
        foreach ($this->element->childNodes as $subNode) {
            assert($subNode instanceof TagRenderer);
            $content .= $subNode->render($state);
        }
        $content .= '</' . $tag . '>';
        return $content;
    }
}
