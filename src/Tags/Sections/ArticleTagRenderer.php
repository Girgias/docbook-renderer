<?php

namespace Girgias\DocbookRender\Tags\Sections;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\State\HierarchicalState;
use Girgias\DocbookRender\TagRenderer;
use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;

class ArticleTagRenderer extends SectionTagRenderer implements TagRenderer
{
    public function render(RendererState $state): string
    {
        if ($this->element->firstElementChild->tagName === "info") {
            throw new \Exception("Does not support info tag yet");
        }
        return parent::render($state);
    }
}
