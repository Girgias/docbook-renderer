<?php

namespace Girgias\DocbookRender\Tags\Sections;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;
use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;

/**
 * This class is used to render <note>, <caution>, <tip>, <warning> tags.
 */
class AlertRenderer implements TagRenderer
{
    private readonly string $id;
    public function __construct(
        protected readonly \DOMElement&TagRenderer $element,
        /** @param array<string> $classes */
        private array $classes = ['alert'],
        string $id = '',
    ) {
        if ($id === '' && $this->element->hasAttribute('xml:id')) {
            $id = $element->getAttribute('xml:id');
        }
        $this->id = $id;
    }

    public function render(RendererState $state): string
    {
        $id = '';
        if ($this->id) {
            $id = ' id="' . $this->id . '"';
        }
        $type = match ($this->element->tagName) {
            'warning', 'tip', 'note', 'caution' => $this->element->tagName,
        };
        $this->classes[] = $type;
        $classes = ' class="' . implode(' ', $this->classes) . '"';

        $content = '<div' . $id . $classes . '>';

        /* "Inline rendering" */
        if (count($this->element->childNodes) === 1 && $this->element->childNodes[0]->tagName === 'simpara') {
            $content .= '<p><strong>' . ucfirst($type) . '</strong>:';
            $content .= (new SimpleReplacementTagRender($this->element->childNodes[0], 'span', [$type]))
                ->render($state);
            $content .= '</p>';
        } else {
            $content .= '<p><strong>' . ucfirst($type) . '</strong>:</p>';
            foreach ($this->element->childNodes as $subNode) {
                $content .= $subNode->render($state);
            }
        }
        $content .= '</div>';
        return $content;
    }
}
