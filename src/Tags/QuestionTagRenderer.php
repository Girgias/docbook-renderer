<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\TagRenderer;

class QuestionTagRenderer extends SimpleReplacementTagRender implements TagRenderer
{
    public function __construct(\DOMElement&TagRenderer $element, string $id)
    {
        parent::__construct($element, 'dt', id: $id);
    }

    public function getQuestionText(): string
    {
        return $this->element->textContent;
    }
}
