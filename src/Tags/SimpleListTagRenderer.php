<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

/**
 * A SimpleList turns out to be anything but simple: https://tdg.docbook.org/tdg/5.1/simplelist.html
 * We take the liberty that if no column attribute is present to display it like a list instead of a table
 */
class SimpleListTagRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element)
    {
    }

    public function render(RendererState $state): string
    {
        $members = [];
        foreach ($this->element->childNodes as $subNode) {
            assert($subNode instanceof TagRenderer);
            assert($subNode instanceof \DOMElement);
            assert($subNode->tagName === 'member');
            $members[] = $subNode->render($state);
        }

        $cols = 0;
        $type = null;
        if ($this->element->hasAttribute('columns')) {
            $cols = (int) $this->element->getAttribute('columns');
            assert($cols >= 1);
        }
        if ($this->element->hasAttribute('type')) {
            $type = $this->element->getAttribute('type');
        }

        $transformsMembers = match ($type) {
            null => '<ul>' . implode('', array_map(fn (string $member) => "<li>$member</li>", $members)) . '</ul>',
            'vert' => $this->processVertical($members, $cols),
            'horiz' => $this->processHoriz($members, $cols),
            'inline' => implode(', ', $members),
        };
        return $transformsMembers;
    }

    private function processTabular(array $members, int $cols): array
    {
        return array_map(
            fn (string $member) => "<td>$member</td>",
            /** The padding is done by getting the additive modular inverse which is
             * ``-nb_arr mod columns`` but because PHP gives us the mod in negative we need to
             * add $cols back to get the positive
             */
            [...$members, ...array_fill(0, (-\count($members) % $cols) + $cols, '')]
        );
    }

    private function chunkReduceTable(array $members, int $cols): string
    {
        return '<table>' .
            array_reduce(
                array_chunk(
                    $members,
                    $cols,
                ),
                fn (string $carry, array $entry) => $carry . '<tr>' . implode('', $entry) . '</tr>',
                ''
            )
            . '</table>';
    }

    private function processHoriz(array $members, int $cols): string
    {
        return $this->chunkReduceTable($this->processTabular($members, $cols), $cols);
    }

    private function processVertical(array $members, int $cols): string
    {
        $members = $this->processTabular($members, $cols);
        // Sort elements so that we get each correct element for the rows to display vertically
        uksort($members, fn (int $l, int $r) => $l % $cols <=> $r % $cols);
        return $this->chunkReduceTable($members, $cols);
    }
}
