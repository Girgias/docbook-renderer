<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class DropTagRenderer implements TagRenderer
{
    public function __construct(
        private readonly \DOMElement&TagRenderer $element,
    ) {
    }

    public function render(RendererState $state): string
    {
        $content = '';
        foreach ($this->element->childNodes as $subNode) {
            assert($subNode instanceof TagRenderer);
            $content .= $subNode->render($state);
        }
        return $content;
    }
}
