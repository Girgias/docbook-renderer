<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class SimpleReplacementTagRender implements TagRenderer
{
    private readonly array $classes;
    private readonly string $id;

    /** @param array<string> $classes */
    public function __construct(
        protected readonly \DOMElement&TagRenderer $element,
        private readonly string $newTag,
        array $classes = [],
        string $id = '',
    ) {
        if ($id === '' && $this->element->hasAttribute('xml:id')) {
            $id = $element->getAttribute('xml:id');
        }
        $this->id = $id;
        $this->classes = [$this->element->tagName, ...$classes];
    }

    public function render(RendererState $state): string
    {
        $id = '';
        if ($this->id) {
            $id = ' id="' . $this->id . '"';
        }
        $classes = '';
        if ($this->classes) {
            $classes = ' class="' . implode(' ', $this->classes) . '"';
        }
        $content = '<' . $this->newTag . $id . $classes . '>';
        foreach ($this->element->childNodes as $subNode) {
            assert($subNode instanceof TagRenderer);
            $content .= $subNode->render($state);
        }
        $content .= '</' . $this->newTag . '>';
        return $content;
    }
}
