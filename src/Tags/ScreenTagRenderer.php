<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class ScreenTagRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement $element)
    {
    }

    public function render(RendererState $state): string
    {
        $content = '';
        assert(count($this->element->childNodes) === 1);
        assert($this->element->firstChild instanceof \DOMCdataSection);
        $content .= '<pre><samp>';
        $content .= htmlspecialchars(
            ltrim($this->element->firstChild->textContent, "\r\n"),
            ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5
        );
        $content .= '</samp></pre>';
        return $content;
    }
}
