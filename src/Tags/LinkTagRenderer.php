<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class LinkTagRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element)
    {
    }

    public function render(RendererState $state): string
    {
        if ($this->element->hasAttribute('linkend')) {
            throw new \Error("No support for <link> tags that use linkend attribute to cross-reference yet");
        }
        if ($this->element->hasAttribute('xlink:href')) {
            $link = $this->element->getAttribute('xlink:href');
            $content = '<a href="' . $link . '">';
            if ($this->element->firstChild) {
                foreach ($this->element->childNodes as $subNode) {
                    assert($subNode instanceof TagRenderer);
                    $content .= $subNode->render($state);
                }
            } else {
                $content .= $link;
            }
            $content .= '</a>';
            return $content;
        }
        throw new \ValueError("<link> tags must either have linkend or xlink:href attribute");
    }
}
