<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class QandARenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element)
    {
        assert($this->element->tagName === 'qandaset');
    }

    public function render(RendererState $state): string
    {
        $entriesRender = '';
        $tocRender = "<ol class='qandaset_questions'>\n";
        foreach ($this->element->childNodes as $node) {
            if ($node instanceof \DOMText) {
                $entriesRender .= $node->textContent;
                continue;
            }
            assert($node instanceof \DOMElement);
            assert($node instanceof TagRenderer);
            if ($node->tagName === 'qandadiv') {
                // TODO Support for qandadiv?
                continue;
            }
            [$id, $question, $render] = $this->renderEntry($node, $state);
            $entriesRender .= $render;
            $tocRender .= "<li><a href='#$id'>$question</a></li>\n";
        }
        $tocRender .= "</ol>\n";
        return "<div class='qandaset'>\n$tocRender<dl>$entriesRender</dl></div>";
    }

    /**
     * @return array{string, string, string}
     */
    public function renderEntry(\DOMElement&TagRenderer $entry, RendererState $state): array
    {
        assert($entry->tagName === 'qandaentry');
        $id = $entry->getAttribute('xml:id');

        $entryRenderer = '';
        foreach ($entry->childNodes as $subNode) {
            if ($subNode->tagName === 'question') {
                $questionRender = new QuestionTagRenderer($subNode, $id);
                $questionText = trim($questionRender->getQuestionText());
                $entryRenderer .= $questionRender->render($state);
                continue;
            }
            assert($subNode->tagName === 'anwser');
            $entryRenderer .= (new SimpleReplacementTagRender($subNode, 'dd'))->render($state);
        }

        // Get question text separately as to generate a ToC
        return [$id, $questionText, $entryRenderer];
    }
}
