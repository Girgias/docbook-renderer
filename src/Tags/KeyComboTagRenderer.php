<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class KeyComboTagRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element)
    {
    }

    public function render(RendererState $state): string
    {
        $combo = '<kbd class="keycombo">';
        $childNodes = iterator_to_array($this->element->childNodes);
        $childNodes = array_map(
            fn (\DOMElement&TagRenderer $node) => $node->render($state),
            $childNodes
        );
        $combo .= implode('+', $childNodes);
        $combo .= '</kbd>';
        return  $combo;
    }
}
