<?php

namespace Girgias\DocbookRender\Tags;

/* NOTE:
Accessibility concerns

Spelling out the acronym or abbreviation in full the first time it is used on a page is beneficial
for helping people understand it, especially if the content is technical or industry jargon.

Only include a title if expanding the abbreviation or acronym in the text is not possible.
Having a difference between the announced word or phrase and what is displayed on the screen,
especially if it's technical jargon the reader may not be familiar with, can be jarring.
https://developer.mozilla.org/en-US/docs/Web/HTML/Element/abbr#accessibility_concerns
*/

use DOMElement;
use DOMText;
use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class AcronymTagRender implements TagRenderer
{
    /**
     * @param array<string, string> $meanings
     */
    public function __construct(private readonly DOMElement $element, private readonly array $meanings)
    {
    }

    public function render(RendererState $state): string
    {
        if ($this->element->childNodes->length !== 1) {
            trigger_error('Acronym tag has multiple child nodes', E_USER_ERROR);
        }
        $text = $this->element->childNodes->item(0);
        if (!($text instanceof DOMText)) {
            trigger_error('Acronym tag must only have text in it', E_USER_ERROR);
        }
        $acronym = $text->textContent;
        if (!array_key_exists($acronym, $this->meanings)) {
            trigger_error("Acronym $acronym has no provided meaning", E_USER_NOTICE);
            return '<abbr>' . $acronym . '</abbr>';
        }
        return '<abbr title="' . $this->meanings[$acronym] . '">' . $acronym . '</abbr>';
    }
}
