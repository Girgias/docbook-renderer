<?php

namespace Girgias\DocbookRender\Tags\Table;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class TableSectionRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element, private readonly bool $headerCells = false)
    {
    }
    public function render(RendererState $state): string
    {
        $content = '<' . $this->element->tagName . '>';
        foreach ($this->element->childNodes as $node) {
            assert($node instanceof TagRenderer);
            assert($node instanceof \DOMElement);
            assert($node->tagName == 'row');
            $content .= (new RowTagRenderer($node, $this->headerCells))->render($state);
        }
        $content .= '</' . $this->element->tagName . '>';
        return $content;
    }
}
