<?php

namespace Girgias\DocbookRender\Tags\Table;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;
use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;

class RowTagRenderer implements TagRenderer
{
    private readonly string $cellTag;
    public function __construct(private readonly \DOMElement&TagRenderer $element, bool $headerCells = false)
    {
        assert($this->element->tagName === 'row');
        if ($headerCells) {
            $this->cellTag = 'th';
        } else {
            $this->cellTag = 'td';
        }
    }

    public function render(RendererState $state): string
    {
        $row = '<tr>';
        foreach ($this->element->childNodes as $node) {
            assert($node instanceof TagRenderer);
            assert($node instanceof \DOMElement);
            assert($node->tagName == 'entry');
            $row .= (new SimpleReplacementTagRender($node, $this->cellTag))->render($state);
        }
        $row .= '</tr>';
        return  $row;
    }
}
