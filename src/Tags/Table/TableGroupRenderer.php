<?php

namespace Girgias\DocbookRender\Tags\Table;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class TableGroupRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element)
    {
        assert($this->element->tagName === 'tgroup');
    }

    public function render(RendererState $state): string
    {
        $head = '';
        $body = '';
        $foot = '';
        foreach ($this->element->childNodes as $node) {
            assert($node instanceof TagRenderer);
            assert($node instanceof \DOMElement);
            if ($node->tagName == 'thead') {
                if ($head !== '') {
                    throw new \Exception('Cannot have multiple thead in table');
                }
                $head = (new TableSectionRenderer($node, true))->render($state);
            } elseif ($node->tagName == 'tfoot') {
                if ($foot !== '') {
                    throw new \Exception('Cannot have multiple thead in table');
                }
                $foot = (new TableSectionRenderer($node, true))->render($state);
            } else {
                assert($node->tagName == 'tbody');
                $body .= (new TableSectionRenderer($node, false))->render($state);
            }
        }
        return $head . $body . $foot;
    }
}
