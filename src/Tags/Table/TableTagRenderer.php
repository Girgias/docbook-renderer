<?php

namespace Girgias\DocbookRender\Tags\Table;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;
use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;

class TableTagRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement&TagRenderer $element)
    {
        assert($this->element->tagName === 'table');
    }

    public function render(RendererState $state): string
    {
        $table = '<table>';
        foreach ($this->element->childNodes as $node) {
            assert($node instanceof TagRenderer);
            assert($node instanceof \DOMElement);
            $renderer = match ($node->tagName) {
                'title' => new SimpleReplacementTagRender($node, 'caption'),
                'tgroup' => new TableGroupRenderer($node)
            };
            $table .= $renderer->render($state);
        }
        $table .= '</table>';
        return $table;
    }
}
