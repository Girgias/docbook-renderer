<?php

namespace Girgias\DocbookRender\Tags;

use DOMElement;
use DOMText;
use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class TypeTagRendererWithoutLinking implements TagRenderer
{
    public function __construct(private readonly DOMElement $element, private readonly bool $isNested = false)
    {
        assert($this->element->tagName === 'type');
    }

    public function render(RendererState $state): string
    {
        /* Single type */
        if ($this->element->attributes->length === 0) {
            if ($this->element->childNodes->length !== 1) {
                trigger_error('Type tag with no attributes must be a single type', E_USER_ERROR);
            }
            $text = $this->element->childNodes->item(0);
            if (!($text instanceof DOMText)) {
                trigger_error('Single type tag must only have text in it', E_USER_ERROR);
            }
            $type = $text->textContent;
            return '<span class="type">' . $type . '</span>';
        }
        /* Compound type */
        if ($this->element->attributes->length !== 1) {
            trigger_error(
                'Composite Type tag must have an attribute to tell if it\'s a union or intersection',
                E_USER_ERROR
            );
        }
        $attribute = $this->element->attributes->item(0);
        assert($attribute->nodeName === 'class');
        $separator = match ($attribute->nodeValue) {
            'union' => '|',
            'intersection' => '&amp;',
        };
        $renderedTypes = array_map(
            fn (DOMElement $v) => (new TypeTagRendererWithoutLinking($v, true))->render($state),
            iterator_to_array($this->element->childNodes)
        );
        if ($this->isNested) {
            //return '(<span class="type">' . implode($separator, $renderedTypes) . '</span>)';
            return '(' . implode($separator, $renderedTypes) . ')';
        }
        return '<span class="type">' . implode($separator, $renderedTypes) . '</span>';
    }
}
