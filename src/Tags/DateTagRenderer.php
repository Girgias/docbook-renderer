<?php

namespace Girgias\DocbookRender\Tags;

use Girgias\DocbookRender\RendererState;
use Girgias\DocbookRender\TagRenderer;

class DateTagRenderer implements TagRenderer
{
    public function __construct(private readonly \DOMElement $element)
    {
    }

    // DocBook does not dictate the format, so validation + HTML escaping should be done?
    public function render(RendererState $state): string
    {
        $nodeText = $this->element->textContent;
        return '<time datetime="' . $nodeText . '">' . $nodeText . '</time>';
    }
}
