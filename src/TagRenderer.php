<?php

namespace Girgias\DocbookRender;

interface TagRenderer
{
    public function render(RendererState $state): string;
}
