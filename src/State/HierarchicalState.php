<?php

namespace Girgias\DocbookRender\State;

use Girgias\DocbookRender\RendererState;

class HierarchicalState implements RendererState
{
    public const HIERARCHICAL_LEVEL = 'level';

    private array $states = [
        self::HIERARCHICAL_LEVEL => 0,
    ];
    public function get(string $key): mixed
    {
        return $this->states[$key];
    }

    public function update(string $key, mixed $data): void
    {
        $this->states[$key] = $data;
    }
}
