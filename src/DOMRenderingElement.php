<?php

namespace Girgias\DocbookRender;

class DOMRenderingElement extends \DOMElement implements TagRenderer
{
    /** @var pure-Closure(\DOMElement $element):TagRenderer */
    public static \Closure $getTagRenderer;

    public function render(RendererState $state): string
    {
        $renderer = $this::$getTagRenderer;
        return $renderer($this)->render($state);
    }
}
