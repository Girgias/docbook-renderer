<?php

namespace Girgias\DocbookRender;

interface RendererState
{
    public function get(string $key): mixed;

    public function update(string $key, mixed $data): void;
}
