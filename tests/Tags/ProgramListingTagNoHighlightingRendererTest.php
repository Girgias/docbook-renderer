<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class ProgramListingTagNoHighlightingRendererTest extends TestCase
{
    public function testBasicListing(): void
    {
        $xml = <<<'XML'
 <programlisting role="php">
<![CDATA[
<?php
$array1 = array("a" => "green", "red", "blue");
$array2 = array("b" => "green", "yellow", "red");
$result = array_intersect($array1, $array2);
print_r($result);
?>
]]>
 </programlisting>
XML;
        $expected = <<<'EXPECTED'
<pre><code class="language-php">&lt;?php
$array1 = array("a" =&gt; "green", "red", "blue");
$array2 = array("b" =&gt; "green", "yellow", "red");
$result = array_intersect($array1, $array2);
print_r($result);
?&gt;
</code></pre>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testProgrammingListingInPara(): void
    {
        $xml = <<<'XML'
<para>
 <programlisting role="shell">
<![CDATA[
git fetch REMOTE pull/$ID/head:$BRANCHNAME
]]>
 </programlisting>
</para>
XML;
        $expected = <<<'EXPECTED'
<p class="para">
 <pre>
  <code class="language-shell">git fetch REMOTE pull/$ID/head:$BRANCHNAME
</code>
 </pre>
</p>

EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );

    }
}
