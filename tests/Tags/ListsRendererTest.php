<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class ListsRendererTest extends TestCase
{
    public function testItemizedList(): void
    {
        $xml = <<<'XML'
<itemizedlist>
 <listitem>
  <simpara>
   Milk
  </simpara>
 </listitem>
 <listitem>
  <simpara>
   Eggs
  </simpara>
 </listitem>
 <listitem>
  <simpara>
   Tea
  </simpara>
 </listitem>
</itemizedlist>
XML;
        /* See https://www.hawkeslearning.com/Accessibility/guides/html_content.html#list-content as to why
         * we have <p> elements within a <li> */
        $expected = <<<'EXPECTED'
<ul class="itemizedlist">
 <li class="listitem">
  <p class="simpara">
   Milk
  </p>
 </li>
 <li class="listitem">
  <p class="simpara">
   Eggs
  </p>
 </li>
 <li class="listitem">
  <p class="simpara">
   Tea
  </p>
 </li>
</ul>
EXPECTED;


        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
    public function testOrderedList(): void
    {
        $xml = <<<'XML'
<orderedlist>
 <listitem>
  <simpara>
   Milk
  </simpara>
 </listitem>
 <listitem>
  <simpara>
   Eggs
  </simpara>
 </listitem>
 <listitem>
  <simpara>
   Tea
  </simpara>
 </listitem>
</orderedlist>
XML;
        /* See https://www.hawkeslearning.com/Accessibility/guides/html_content.html#list-content as to why
         * we have <p> elements within a <li> */
        $expected = <<<'EXPECTED'
<ol class="orderedlist">
 <li class="listitem">
  <p class="simpara">
   Milk
  </p>
 </li>
 <li class="listitem">
  <p class="simpara">
   Eggs
  </p>
 </li>
 <li class="listitem">
  <p class="simpara">
   Tea
  </p>
 </li>
</ol>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
