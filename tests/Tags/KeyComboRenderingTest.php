<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class KeyComboRenderingTest extends TestCase
{
    public function testSimulKeyCombo(): void
    {
        $xml = <<<'XML'
<keycombo action="simul"><keycap>CTRL</keycap><keycap>C</keycap></keycombo>
XML;
        /* See https://www.hawkeslearning.com/Accessibility/guides/html_content.html#list-content as to why
         * we have <p> elements within a <li> */
        $expected = <<<'EXPECTED'
<kbd class="keycombo"><kbd class="keycap">CTRL</kbd>+<kbd class="keycap">C</kbd></kbd>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
    public function testDefaultKeyCombo(): void
    {
        $xml = <<<'XML'
<keycombo><keycap>CTRL</keycap><keycap>C</keycap></keycombo>
XML;
        /* See https://www.hawkeslearning.com/Accessibility/guides/html_content.html#list-content as to why
         * we have <p> elements within a <li> */
        $expected = <<<'EXPECTED'
<kbd class="keycombo"><kbd class="keycap">CTRL</kbd>+<kbd class="keycap">C</kbd></kbd>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
