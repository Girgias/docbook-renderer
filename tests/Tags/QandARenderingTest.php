<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class QandARenderingTest extends TestCase
{
    public function testSimpleQandASet(): void
    {
        $xml = <<<'XML'
<qandaset>
 <qandaentry xml:id="faq.x">
  <question>
   <para>What is X?</para>
  </question>
  <answer>
   <para>
    Some answer to X.
   </para>
  </answer>
 </qandaentry>

 <qandaentry xml:id="faq.y">
  <question>
   <para>What is Y?</para>
  </question>
  <answer>
   <para>
    Some answer to Y.
   </para>
   <para>
    More details about Y.
   </para>
  </answer>
 </qandaentry>
</qandaset>
XML;
        $expected = <<<'EXPECTED'
<div class='qandaset'>
 <ol class='qandaset_questions'>
  <li><a href='#faq.x'>What is X?</a></li>
  <li><a href='#faq.y'>What is Y?</a></li>
 </ol>

 <dl>
  <dt class="question" id="faq.x">
   <p class="para">What is X?</p>
  </dt>
  <dd class="answer">
   <p class="para">
    Some answer to X.
   </p>
  </dd>

  <dt class="question" id="faq.y">
   <p class="para">What is Y?</p>
  </dt>
  <dd class="answer">
   <p class="para">
    Some answer to Y.
   </p>
   <p class="para">
    More details about Y.
   </p>
  </dd>
 </dl>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
