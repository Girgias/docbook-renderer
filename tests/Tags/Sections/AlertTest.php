<?php

namespace Tags\Sections;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class AlertTest extends TestCase
{
    public function testSimpleInlineNote(): void
    {
        $xml = <<<'XML'
<note xmlns='http://docbook.org/ns/docbook'>
 <simpara>This is a note</simpara>
</note>
XML;
        $expected = <<<'EXPECTED'
<div class="alert note">
 <p><strong>Note</strong>:<span class="simpara note">This is a note</span></p>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testInlineNoteWithTags(): void
    {
        $xml = <<<'XML'
<note xmlns='http://docbook.org/ns/docbook'>
 <simpara>This is a note with <emphasis>important</emphasis> information.</simpara>
</note>
XML;
        $expected = <<<'EXPECTED'
<div class="alert note">
    <p><strong>Note</strong>:<span class="simpara note">This is a note with <em class="emphasis">important</em> information.</span></p>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testBlockNoteWithTags(): void
    {
        $xml = <<<'XML'
<note xmlns='http://docbook.org/ns/docbook'>
 <para>This is a note with <emphasis>important</emphasis> information.</para>
</note>
XML;
        $expected = <<<'EXPECTED'
<div class="alert note">
    <p><strong>Note</strong>:</p>
    <p class="para">This is a note with <em class="emphasis">important</em> information.</p>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testSimpleInlineWarning(): void
    {
        $xml = <<<'XML'
<warning xmlns='http://docbook.org/ns/docbook'>
 <simpara>This is a message</simpara>
</warning>
XML;
        $expected = <<<'EXPECTED'
<div class="alert warning">
 <p><strong>Warning</strong>:<span class="simpara warning">This is a message</span></p>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testSimpleInlineCaution(): void
    {
        $xml = <<<'XML'
<caution xmlns='http://docbook.org/ns/docbook'>
 <simpara>This is a message</simpara>
</caution>
XML;
        $expected = <<<'EXPECTED'
<div class="alert caution">
 <p><strong>Caution</strong>:<span class="simpara caution">This is a message</span></p>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testSimpleInlineTip(): void
    {
        $xml = <<<'XML'
<tip xmlns='http://docbook.org/ns/docbook'>
 <simpara>This is a message</simpara>
</tip>
XML;
        $expected = <<<'EXPECTED'
<div class="alert tip">
 <p><strong>Tip</strong>:<span class="simpara tip">This is a message</span></p>
</div>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
