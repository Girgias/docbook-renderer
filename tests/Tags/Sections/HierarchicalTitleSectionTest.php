<?php

namespace Tags\Sections;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class HierarchicalTitleSectionTest extends TestCase
{
    public function testBasicSectioning(): void
    {
        $xml = <<<'XML'
<section xmlns='http://docbook.org/ns/docbook'>
 <title>Level 1 title</title>
 <para>Some text</para>
 <section>
  <title>Level 2 title</title>
  <para>Some text</para>
  <section>
   <title>Level 3 title</title>
   <para>Some text</para>
   <section>
    <title>Level 4 title</title>
    <para>Some text</para>
    <section>
     <title>Level 5 title</title>
     <para>Some text</para>
     <section>
      <title>Level 6 title</title>
      <para>Some text</para>
      <section>
       <title>Level 7 title</title>
       <para>Some text</para>
      </section>
     </section>
    </section>
   </section>
  </section>
 </section>
 <section>
  <title>Second level 2 title</title>
  <para>Test state is not poisoned</para>
 </section>
 <para>Ending text</para>
</section>
XML;
        $expected = <<<'EXPECTED'
<section class="section">
 <h1>Level 1 title</h1>
 <p class="para">Some text</p>
 <section class="section">
  <h2>Level 2 title</h2>
  <p class="para">Some text</p>
  <section class="section">
   <h3>Level 3 title</h3>
   <p class="para">Some text</p>
   <section class="section">
    <h4>Level 4 title</h4>
    <p class="para">Some text</p>
    <section class="section">
     <h5>Level 5 title</h5>
     <p class="para">Some text</p>
     <section class="section">
      <h6>Level 6 title</h6>
      <p class="para">Some text</p>
      <section class="section">
       <h6>Level 7 title</h6>
       <p class="para">Some text</p>
      </section>
     </section>
    </section>
   </section>
  </section>
 </section>
 <section class="section">
  <h2>Second level 2 title</h2>
  <p class="para">Test state is not poisoned</p>
 </section>
 <p class="para">Ending text</p>
</section>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
