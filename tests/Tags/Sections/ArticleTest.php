<?php

namespace Tags\Sections;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class ArticleTest extends TestCase
{
    public function testSimpleArticle(): void
    {
        $xml = <<<'XML'
<article xmlns='http://docbook.org/ns/docbook'>
 <title>Main title</title>
 <para>Some text</para>
 <section>
  <title>Level 2 title</title>
  <para>Some text</para>
 </section>
 <section>
  <title>Second level 2 title</title>
  <para>Test state is not poisoned</para>
 </section>
 <para>Ending text</para>
</article>
XML;
        $expected = <<<'EXPECTED'
<article class="article">
 <h1>Main title</h1>
 <p class="para">Some text</p>
 <section class="section">
  <h2>Level 2 title</h2>
  <p class="para">Some text</p>
 </section>
 <section class="section">
  <h2>Second level 2 title</h2>
  <p class="para">Test state is not poisoned</p>
 </section>
 <p class="para">Ending text</p>
</article>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
