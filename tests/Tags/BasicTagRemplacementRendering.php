<?php

namespace Tags;

use Girgias\DocbookRender\Tags\SimpleReplacementTagRender;
use PHPUnit\Framework\TestCase;

class BasicTagRemplacementRendering extends TestCase
{
    public function testReplacement(): void
    {
        $xml = <<<'XML'
<para>
 Hello <emphasis>world</emphasis>.

 <acronym>HTTP</acronym> is amazing.
</para>
XML;
    }
}
