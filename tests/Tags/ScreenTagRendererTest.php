<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class ScreenTagRendererTest extends TestCase
{
    public function testBasicScreen(): void
    {
        $xml = <<<'XML'
    <screen role="php">
<![CDATA[
Array
(
    [a] => green
    [0] => red
)
]]>
    </screen>
XML;
        $expected = <<<'EXPECTED'
<pre>
<samp>Array
(
    [a] => green
    [0] => red
)
</samp>
</pre>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
