<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class LinkRendererTest extends TestCase
{
    public function testBasicLinkHREFWithoutText(): void
    {
        $xml = <<<'XML'
<link xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://example.com" />
XML;
        $expected = <<<'EXPECTED'
<a href="https://example.com">https://example.com</a>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testBasicLinkHREFWithText(): void
    {
        $xml = <<<'XML'
<link  xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://example.com">A link</link>
XML;
        $expected = <<<'EXPECTED'
<a href="https://example.com">A link</a>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
