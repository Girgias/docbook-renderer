<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class DateTagRendereringTest extends TestCase
{
    public function testSimpleDate(): void
    {
        $xml = '<date>2023-01-29</date>';
        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            '<time datetime="2023-01-29">2023-01-29</time>',
            $content,
        );
    }
}
