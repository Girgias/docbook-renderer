<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class TableRenderingTest extends TestCase
{
    public function testRegularTable(): void
    {
        $xml = <<<'XML'
<table xmlns='http://docbook.org/ns/docbook'>
 <title>Regular Table</title>
 <tgroup cols='4'>
  <thead>
   <row>
    <entry>a1</entry>
    <entry>a2</entry>
    <entry>a3</entry>
    <entry>a4</entry>
   </row>
  </thead>
  <tfoot>
   <row>
    <entry>f1</entry>
    <entry>f2</entry>
    <entry>f3</entry>
    <entry>f4</entry>
   </row>
  </tfoot>
  <tbody>
   <row>
    <entry>b1</entry>
    <entry>b2</entry>
    <entry>b3</entry>
    <entry>b4</entry>
   </row>
   <row>
    <entry>c1</entry>
    <entry>c2</entry>
    <entry>c3</entry>
    <entry>c4</entry>
   </row>
   <row>
    <entry>d1</entry>
    <entry>d2</entry>
    <entry>d3</entry>
    <entry>d4</entry>
   </row>
  </tbody>
 </tgroup>
</table>
XML;
        $expected = <<<'EXPECTED'
<table>
 <caption class="title">Regular Table</caption>
 <thead>
  <tr>
   <th class="entry">a1</th>
   <th class="entry">a2</th>
   <th class="entry">a3</th>
   <th class="entry">a4</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td class="entry">b1</td>
   <td class="entry">b2</td>
   <td class="entry">b3</td>
   <td class="entry">b4</td>
  </tr>
  <tr>
   <td class="entry">c1</td>
   <td class="entry">c2</td>
   <td class="entry">c3</td>
   <td class="entry">c4</td>
  </tr>
  <tr>
   <td class="entry">d1</td>
   <td class="entry">d2</td>
   <td class="entry">d3</td>
   <td class="entry">d4</td>
  </tr>
 </tbody>
 <tfoot>
  <tr>
   <th class="entry">f1</th>
   <th class="entry">f2</th>
   <th class="entry">f3</th>
   <th class="entry">f4</th>
  </tr>
 </tfoot>
</table>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testRegularInformalTable(): void
    {
        $xml = <<<'XML'
<informaltable xmlns='http://docbook.org/ns/docbook'>
 <tgroup cols='2'>
  <thead>
   <row>
    <entry>a1</entry>
    <entry>a2</entry>
   </row>
  </thead>
  <tfoot>
   <row>
    <entry>d1</entry>
    <entry>d2</entry>
   </row>
  </tfoot>
  <tbody>
   <row>
    <entry>b1</entry>
    <entry>b2</entry>
   </row>
   <row>
    <entry>c1</entry>
    <entry>c2</entry>
   </row>
  </tbody>
 </tgroup>
</informaltable>
XML;
        $expected = <<<'EXPECTED'
<table>
 <thead>
  <tr>
   <th class="entry">a1</th>
   <th class="entry">a2</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td class="entry">b1</td>
   <td class="entry">b2</td>
  </tr>
  <tr>
   <td class="entry">c1</td>
   <td class="entry">c2</td>
  </tr>
 </tbody>
 <tfoot>
  <tr>
   <th class="entry">d1</th>
   <th class="entry">d2</th>
  </tr>
 </tfoot>
</table>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
    public function testComplexTable(): void
    {
        $xml = <<<'XML'
<table xml:id="ex.calstable" frame='all' xmlns='http://docbook.org/ns/docbook'>
 <title>Sample CALS Table</title>
 <tgroup cols='5' align='left' colsep='1' rowsep='1'>
  <colspec colname='c1'/>
  <colspec colname='c2'/>
  <colspec colname='c3'/>
  <colspec colnum='5' colname='c5'/>
  <thead>
   <row>
    <entry namest="c1" nameend="c2" align="center">Horizontal Span</entry>
    <entry>a3</entry>
    <entry>a4</entry>
    <entry>a5</entry>
   </row>
  </thead>
  <tfoot>
   <row>
    <entry>f1</entry>
    <entry>f2</entry>
    <entry>f3</entry>
    <entry>f4</entry>
    <entry>f5</entry>
   </row>
  </tfoot>
  <tbody>
   <row>
    <entry>b1</entry>
    <entry>b2</entry>
    <entry>b3</entry>
    <entry>b4</entry>
    <entry morerows='1' valign='middle'><para>Vertical Span</para></entry>
   </row>
   <row>
    <entry>c1</entry>
    <entry namest="c2" nameend="c3" align='center' morerows='1' valign='bottom'>Span Both</entry>
    <entry>c4</entry>
   </row>
   <row>
    <entry>d1</entry>
    <entry>d4</entry>
    <entry>d5</entry>
   </row>
  </tbody>
 </tgroup>
</table>
XML;
        $expected = <<<'EXPECTED'
<table style="border-collapse: collapse;">
 <caption class="title">Sample CALS Table</caption>
 <colgroup>
  <col class="tcol1 align-left">
  <col class="tcol2 align-left">
  <col class="tcol3 align-left">
  <col class="tcol4">
  <col class="tcol5 align-left">
 </colgroup>
 <thead>
  <tr>
   <th colspan="2" align="center">Horizontal Span</th>
   <th class="entry align-left">a3</th>
   <th class="entry align-left">a4</th>
   <th class="entry align-left">a5</th>
  </tr>
 </thead>
 <tfoot>
  <tr>
   <th class="entry align-left">f1</th>
   <th class="entry align-left">f2</th>
   <th class="entry align-left">f3</th>
   <th class="entry align-left">f4</th>
   <th class="entry align-left">f5</th>
  </tr>
 </tfoot>
 <tbody>
  <tr>
   <td class="entry align-left">b1</td>
   <td class="entry align-left">b2</td>
   <td class="entry align-left">b3</td>
   <td class="entry align-left">b4</td>
   <td rowspan="2" class="entry align-middle align-left">
    <p>Vertical Span</p>
   </td>
  </tr>
  <tr>
   <td class="entry align-left">c1</td>
   <td rowspan="2" colspan="2" class="entry align-bottom align-center">Span Both</td>
   <td class="entry align-left">c4</td>
  </tr>
  <tr>
   <td class="entry align-left">d1</td>
   <td class="entry align-left">d4</td>
   <td class="entry align-left">d5</td>
  </tr>
 </tbody>
</table>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);

        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This complex case hasn\'t been handled yet.'
        );
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
