<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class SimpleListRenderingTest extends TestCase
{
    public function testTrueSimpleList(): void
    {
        $xml = <<<'XML'
<simplelist xmlns='http://docbook.org/ns/docbook'>
 <member>A</member>
 <member>B</member>
 <member>C</member>
 <member>D</member>
 <member>E</member>
 <member>F</member>
 <member>G</member>
</simplelist>
XML;
        $expected = <<<'EXPECTED'
<ul>
 <li>A</li>
 <li>B</li>
 <li>C</li>
 <li>D</li>
 <li>E</li>
 <li>F</li>
 <li>G</li>
</ul>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }

    public function testHorizontalTabularSimpleList(): void
    {
        $xml = <<<'XML'
<simplelist type='horiz' columns='3' xmlns='http://docbook.org/ns/docbook'>
 <member>A</member>
 <member>B</member>
 <member>C</member>
 <member>D</member>
 <member>E</member>
 <member>F</member>
 <member>G</member>
</simplelist>
XML;
        $expected = <<<'EXPECTED'
<table>
 <tr>
  <td>A</td>
  <td>B</td>
  <td>C</td>
 </tr>
 <tr>
  <td>D</td>
  <td>E</td>
  <td>F</td>
 </tr>
 <tr>
  <td>G</td>
  <td></td>
  <td></td>
 </tr>
</table>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
    public function testVerticalTabularSimpleList(): void
    {
        $xml = <<<'XML'
<simplelist type='vert' columns='3' xmlns='http://docbook.org/ns/docbook'>
 <member>A</member>
 <member>B</member>
 <member>C</member>
 <member>D</member>
 <member>E</member>
 <member>F</member>
 <member>G</member>
</simplelist>
XML;
        $expected = <<<'EXPECTED'
<table>
 <tr>
  <td>A</td>
  <td>D</td>
  <td>G</td>
 </tr>
 <tr>
  <td>B</td>
  <td>E</td>
  <td></td>
 </tr>
 <tr>
  <td>C</td>
  <td>F</td>
  <td></td>
 </tr>
</table>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
    public function testInlineSimpleList(): void
    {
        $xml = <<<'XML'
<para xmlns='http://docbook.org/ns/docbook'>
 An inline list:
<simplelist type='inline'>
 <member>A</member>
 <member>B</member>
 <member>C</member>
 <member>D</member>
 <member>E</member>
 <member>F</member>
 <member>G</member>
</simplelist>
</para>
XML;
        $expected = <<<'EXPECTED'
<p class="para">
 An inline list:
A, B, C, D, E, F, G
</p>
EXPECTED;

        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            $expected,
            $content,
        );
    }
}
