<?php

namespace Tags;

use Girgias\DocbookRender\DOMRenderingDocument;
use Girgias\DocbookRender\State\HierarchicalState;
use PHPUnit\Framework\TestCase;

class TypeTagRendererNoLinkingTest extends TestCase
{
    public function testSimpleType(): void
    {
        $xml = '<type>int</type>';
        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            '<span class="type">int</span>',
            $content,
        );
    }
    public function testUnionType(): void
    {
        $xml = '<type class="union"><type>DOMAttr</type><type>DOMNameSpaceNode</type><type>false</type></type>';
        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            '<span class="type"><span class="type">DOMAttr</span>|<span class="type">DOMNameSpaceNode</span>|<span class="type">false</span></span>',
            $content,
        );
    }
    public function testIntersectionType(): void
    {
        $xml = '<type class="intersection"><type>Countable</type><type>Traversable</type></type>';
        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        self::assertXmlStringEqualsXmlString(
            '<span class="type"><span class="type">Countable</span>&amp;<span class="type">Traversable</span></span>',
            $content,
        );
    }
    public function testDNFType(): void
    {
        $xml = '<type class="union"><type class="intersection"><type>Countable</type><type>Traversable</type></type><type>DOMAttr</type></type>';
        $d = new DOMRenderingDocument($xml);
        $state = new HierarchicalState();
        $content = $d->render($state);
        // Is this actually what we want to render? Or should the nested intersection be surrounded by a span tag?
        self::assertXmlStringEqualsXmlString(
            '<span class="type">(<span class="type">Countable</span>&amp;<span class="type">Traversable</span>)|<span class="type">DOMAttr</span></span>',
            $content,
        );
    }
}
